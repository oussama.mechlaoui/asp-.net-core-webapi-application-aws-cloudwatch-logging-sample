using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace webapi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class RestAPI : ControllerBase
    {
        readonly ILogger<RestAPI> _log;
        public RestAPI(ILogger<RestAPI> log)
        {
        _log = log;
        }
        [HttpGet]
        public ActionResult<Dictionary<string, string>> Get()
        {
            return new Dictionary<string, string>
            {
                {"name", "oussama"}
               // { "name", Environment.GetEnvironmentVariable("NAME") },
               // { "hostname", Environment.GetEnvironmentVariable("HOSTNAME") }
            };
        }
    }

}
