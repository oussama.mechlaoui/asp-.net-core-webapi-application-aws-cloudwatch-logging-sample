using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Net.Http;
using Amazon.XRay.Recorder.Core;


namespace webapi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    
    public class JsonAPI : ControllerBase
    {
        readonly ILogger<JsonAPI> _log;
        public JsonAPI(ILogger<JsonAPI> log)
        {
        _log = log;
        }
        [HttpGet]
        public async Task<IActionResult> Index()
        { 
            using (var httpClient = new HttpClient())
            {
                using (var response = await httpClient.GetAsync("https://www.mtgjson.com/json/PioneerCards.json"))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    return new JsonResult(apiResponse);
                }
            }
            
        }
    }

}
