#!/bin/bash
docker rm webapi -f;
rm -rf publish;
cd webapi;
dotnet publish -c Release -o ../publish;
cd ..;
docker build -t mechlaoui/webapi . ;
docker push mechlaoui/webapi;
docker run -d -p 80:80 --name webapi mechlaoui/webapi;
